import time
import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

from pages.cockpit_page import CockpitPage
from pages.login_page import LoginPage
import string
import random


@pytest.fixture()
def browser():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    login_page = LoginPage(browser)
    login_page.load()
    login_page.login("administrator@testarena.pl", "sumXQQ72$L")
    yield browser
    browser.quit()


def get_random_string(length):
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))


def test_logout_correctly_displeyed(browser):
    assert browser.find_element(By.CSS_SELECTOR, "[title=Wyloguj]").is_displayed() is True


def test_open_administration(browser):
    cockpit_page = CockpitPage(browser)
    cockpit_page.click_administration()

    assert browser.find_element(By.CSS_SELECTOR, '.content_title').text == 'Projekty'


def test_add_new_project(browser):
    cockpit_page = CockpitPage(browser)
    cockpit_page.click_administration()
    browser.find_element(By.CSS_SELECTOR, '.button_link').click()
    random = get_random_string(6)
    project_name = browser.find_element(By.CSS_SELECTOR, '#name').send_keys(random)
    project_prefix = browser.find_element(By.CSS_SELECTOR, '#prefix').send_keys(random)
    project_save = browser.find_element(By.CSS_SELECTOR, '#save').click()
    time.sleep(2)


def test_open_projects_section(browser):
    cockpit_page = CockpitPage(browser)
    cockpit_page.click_administration()
    browser.find_element(By.CSS_SELECTOR, '.activeMenu').click()


def test_find_project_by_name(browser):
    cockpit_page = CockpitPage(browser)
    cockpit_page.click_administration()
    browser.find_element(By.CSS_SELECTOR, '.activeMenu').click()
    browser.find_element(By.CSS_SELECTOR, '#search').send_keys('4OQA40')
    browser.find_element(By.CSS_SELECTOR, '#j_searchButton').click()
    time.sleep(2)

